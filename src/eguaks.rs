extern crate rusoto_core;
extern crate rusoto_iam;

//stlibs
use std::env;

//3rd party libs
use core::borrow::Borrow;
use rusoto_core::{Region, RusotoError};
use rusoto_iam::{
    AttachUserPolicyRequest, CreatePolicyRequest, CreateUserRequest, DeletePolicyRequest,
    DeleteUserRequest, GetAccessKeyLastUsedRequest, GetAccountSummaryError,
    GetAccountSummaryResponse, Iam, IamClient, ListAccessKeysRequest, ListPoliciesRequest,
    ListUsersRequest, ListUsersResponse, Policy, Tag, User,
};
use std::process::exit;

pub async fn list_users(path: &String) -> Option<Vec<User>> {
    println!("Listing users!");

    let client = IamClient::new(Region::UsEast1);

    let list_users_req = ListUsersRequest {
        marker: None,
        max_items: None,
        path_prefix: Some(path.to_string()),
    };

    match client.list_users(list_users_req).await {
        Ok(result) => {
            println!("Found users, returning data");
            println!("{:?}", result.users);

            Some(result.users)
        }
        Err(error) => {
            println!("Ellol mon!: {:?}", error);
            None
        }
    }
}

pub async fn find_user(user_name: &String, path: &String) -> Option<User> {
    let users = list_users(path).await;

    match users {
        Some(data) => {
            let existing_users: Vec<User> = data
                .into_iter()
                .filter(|user| user.user_name == user_name.to_string())
                .collect();

            existing_users.first().cloned()
        }
        None => None,
    }
}

pub async fn create_user(user_name: &String, tags: &Vec<Tag>, path: &String) -> Option<User> {
    println!("Creating user {}", user_name);
    //    let profile: String = String::from("aws5-iam-admin"); //String::from("aws5-rustoto-demo1");
    //
    //    let key: &str = "AWS_PROFILE";
    //    env::set_var(key, profile);

    let client = IamClient::new(Region::UsEast1);
    println!("Create users!!!");

    let create_user_req = CreateUserRequest {
        path: Some(path.to_string()),
        user_name: user_name.to_string(),
        tags: Some(tags.clone()),
        permissions_boundary: None,
    };

    match client.create_user(create_user_req).await {
        Ok(result) => {
            println!("User created oK!!!");
            result.user
        }
        Err(error) => {
            println!("Failed to create user because of: {}", error);
            None
        }
    }
}

#[tokio::test]
async fn test_crud_user_valid_input_creates_user() {
    let user_name = String::from("rust-12324-test");
    let tags: Vec<Tag> = vec![
        Tag {
            key: String::from("App"),
            value: String::from("rusoto"),
        },
        Tag {
            key: String::from("Purpose"),
            value: String::from("demo"),
        },
        Tag {
            key: String::from("Who"),
            value: String::from("john"),
        },
        Tag {
            key: String::from("Comment"),
            value: String::from("None"),
        },
    ];

    let path = String::from("/rust/");

    let created_user = create_user(&user_name, &tags, &path).await;

    println!("Created user: {:?}", created_user);

    assert!(created_user.is_some());

    let existing_user = find_user(&user_name, &path).await;

    assert!(existing_user.is_some());

    delete_user(&user_name).await;

    let deleted_user = find_user(&user_name, &path).await;

    assert!(deleted_user.is_none());
}

pub async fn delete_user(user_name: &String) {
    println!("Creating users, world!");

    let client = IamClient::new(Region::UsEast1);
    println!("Deleting user with name: {}", user_name);

    let request = DeleteUserRequest {
        user_name: user_name.to_string(),
    };

    match client.delete_user(request).await {
        Ok(result) => {
            println!("Users deleted!!!");
        }
        Err(error) => {
            println!("Failed to dele user: {}", error);
        }
    }
}

pub async fn create_user_iam_policy(
    policy_name: &String,
    path: &String,
    user_arn: &String,
) -> Option<Policy> {
    let client = IamClient::new(Region::UsEast1);

    let raw_policy_document = String::from(
        r#"
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "iam:ListSSHPublicKeys",
                "iam:ListGroupsForUser",
                "iam:UploadSSHPublicKey",
                "iam:UpdateAccessKey",
                "iam:DeleteSSHPublicKey",
                "iam:UpdateSSHPublicKey",
                "iam:ListUserPolicies",
                "iam:ListMFADevices",
                "iam:GetLoginProfile",
                "iam:ListUserTags",
                "iam:CreateAccessKey",
                "iam:ListAccessKeys",
                "iam:DeleteAccessKey"
            ],
            "Resource": "|PLACEHOLDER|"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "iam:ListPolicies",
                "iam:ListAccountAliases",
                "iam:ListRoles",
                "iam:ListGroups",
                "iam:GetAccountSummary"
            ],
            "Resource": "*"
        }
    ]
}
"#,
    );

    let policy_document = raw_policy_document.replace("|PLACEHOLDER|", user_arn);

    println!("Policy :{}", policy_document);

    let request = CreatePolicyRequest {
        description: Some(String::from("Standard iam policy for users")),
        path: Some(path.to_string()),
        policy_document: policy_document.trim().to_string(),
        policy_name: policy_name.to_string(),
    };

    println!("Creating policy document, with name: {}", policy_name);

    match client.create_policy(request).await {
        Ok(result) => {
            println!("Created policy document ok!!!!!!!!!!! {:?}", result);
            println!("Attaching policy to user");
            result.policy
        }
        Err(error) => {
            println!("Failed to create policy document:  {}", error);
            None
        }
    }
}

pub async fn attach_user_policy(policy_arn: &String, user_name: &String) {
    println!("Attaching user policy");

    let client = IamClient::new(Region::UsEast1);
    println!("Deleting user with name: {}", user_name);

    let req = AttachUserPolicyRequest {
        policy_arn: policy_arn.to_string(),
        user_name: user_name.to_string(),
    };

    match client.attach_user_policy(req).await {
        Ok(result) => {
            println!("Attached policy to user {}", user_name);
        }
        Err(error) => {
            println!(
                "Failed to attach policy to user {} because of {}",
                user_name, error
            );
        }
    }
}

pub async fn create_user_list_iam_policy(policy_name: &String, path: &String) {
    let client = IamClient::new(Region::UsEast1);

    let list_policy = String::from(
        r#"
{
    "Version": "2012-10-17",
    "Statement": {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "iam:ListUsers",
            "Resource": "*"
        }
}
"#,
    );

    println!("Policy :{}", list_policy);

    let request = CreatePolicyRequest {
        description: Some(String::from("Standard iam policy for users")),
        path: Some(path.to_string()),
        policy_document: list_policy.trim().to_string(),
        policy_name: policy_name.to_string(),
    };

    println!("Creating policy document, with name: {}", policy_name);

    match client.create_policy(request).await {
        Ok(result) => {
            println!("Created policy document ok!!!!!!!!!!! {:?}", result);
        }
        Err(error) => {
            println!("Failed to create policy document:  {}", error);
        }
    }
}

pub async fn delete_policy(policy_name: &String) {
    let client = IamClient::new(Region::UsEast1);

    let list_request = ListPoliciesRequest {
        marker: None,
        max_items: None,
        only_attached: None,
        path_prefix: Some(String::from("/rust/")),
        policy_usage_filter: None,
        scope: None,
    };

    match client.list_policies(list_request).await {
        Ok(result) => match result.policies {
            Some(policies) => {
                println!("Found: {:?}", policies);
                for policy in policies {
                    match policy.policy_name {
                        Some(p) if &p == policy_name => {
                            println!("Found match with name: {:?}", p);
                            println!("Deleting it");
                            let request = DeletePolicyRequest {
                                policy_arn: match policy.arn {
                                    Some(arn) => arn,
                                    _ => String::from(""),
                                },
                            };

                            match client.delete_policy(request).await {
                                Ok(result) => {
                                    println!("Deleted policy with name: {}", p);
                                }
                                Err(error) => {
                                    println!("Failed to delete policy with name: {}", p);
                                }
                            }
                        }
                        _ => {
                            println!("Skipping!");
                        }
                    }
                }
            }
            None => {
                println!("No matching policies");
            }
        },
        Err(error) => {
            println!("Failed to list policies!");
        }
    }

    //    let request = DeletePolicyRequest {
    //        policy_arn: "".to_string()
    //    };
    //
    //    client.delete_policy()
}
