mod eguaks;

extern crate rusoto_core;
extern crate rusoto_iam;

//stlibs
use std::env;

//3rd party libs
use core::borrow::Borrow;
use rusoto_core::{Region, RusotoError};
use rusoto_iam::{
    CreatePolicyRequest, CreateUserRequest, DeletePolicyRequest, DeleteUserRequest,
    GetAccessKeyLastUsedRequest, GetAccountSummaryError, GetAccountSummaryResponse, Iam, IamClient,
    ListAccessKeysRequest, ListPoliciesRequest, ListUsersRequest, ListUsersResponse, Tag, User,
};

#[tokio::main]
async fn main() {
    let path = String::from("/rust/");
    let users = vec![String::from("rust-user1"), String::from("rust-user2")];
    let tags: Vec<Tag> = vec![
        Tag {
            key: String::from("App"),
            value: String::from("rusoto"),
        },
        Tag {
            key: String::from("Purpose"),
            value: String::from("demo"),
        },
        Tag {
            key: String::from("Who"),
            value: String::from("john"),
        },
        Tag {
            key: String::from("Comment"),
            value: String::from("None"),
        },
    ];

    for user_name in users {
        match eguaks::create_user(&user_name, &tags, &path).await {
            Some(user) => {
                println!("Creating user policy");
                let policy_name = String::from(user.user_name.to_string() + "iam-policy");
                let user_name = user.user_name.to_string();
                let user_arn = user.arn.to_string();
                match eguaks::create_user_iam_policy(&policy_name, &path, &user_arn).await {
                    Some(policy) => match policy.arn {
                        Some(arn) => {
                            println!("Attaching policy to user!");
                            eguaks::attach_user_policy(&arn, &user_name).await;
                        }
                        None => {
                            println!("Missing arn!!");
                        }
                    },
                    None => {
                        println!("Failed to create policy!");
                    }
                }
            }
            None => {
                println!("User not created!");
            }
        }
    }

    match eguaks::list_users(&path).await {
        Some(result) => {
            //            for user in result {
            //                println!("User: {:?}", user.user_name);
            //                delete_user(user.user_name.borrow()).await;
            //            }
        }
        None => {
            println!("Did not find users!");
        }
    }
}
